#include <sstream>
#include <algorithm>
#include "../includes/array_functions.h"
#include "../includes/utilities.h"

using namespace std;
using namespace constants;

namespace KP{
	//zero out vector that tracks words and their occurrences
	void clear(vector<entry>  &entries) {
		entries.clear();
	}

	//how many unique words are in the vector
	int getSize(vector<entry>  &entries) {
		return entries.size();
	}

	//get data at a particular location, if i>size() then get the last value in the vector
	//(this is lazy, should throw an exception instead)
	string getWordAt(vector<entry>  &entries, int i) {
		if (  (unsigned int) i >= entries.size()) {
			throw FAIL;
		}
		return entries[i].word;
	}

	int getNumbOccurAt(vector<entry>  &entries, int i) {
		if ( (unsigned int) i >= entries.size()) {
			throw FAIL;
		}
		return entries[i].number_occurences;
	}

	/*loop through whole file, one line at a time
	 * call processLine on each line from the file
	 * returns false: myfstream is not open
	 *         true: otherwise*/
	bool processFile(vector<entry>  &entries,fstream &myfstream) {
		if (!myfstream.is_open()) {
			return false; }

		string line = "";

		while (!myfstream.eof()) {
			getline(myfstream, line);
			processLine(entries, line);
		}

		return true;
	}

	/*take 1 line and extract all the tokens from it
	feed each token to processToken for recording*/
	void processLine(vector<entry>  &entries,string &myString) {
		string token = "";
		stringstream line;
		line.str(myString);

		while (!line.eof()) {
			line >> token;
			processToken(entries, token);
			token = "";
		}
	}

	/*Keep track of how many times each token seen*/
	void processToken(vector<entry>  &entries,string &token) {
		strip_unwanted_chars(token);
		if (token.length() == 0)
			return;
		string caps = token;
		toUpper(caps);
		int i, size = entries.size();

		for (i = 0; i < size; ++i) {
			if (entries[i].word_uppercase == caps) {
				entries[i].number_occurences += 1;
				break;
			}
		}

		if (i == size) {
			entry newEntry;
			newEntry.word = token;
			newEntry.word_uppercase = caps;
			newEntry.number_occurences = 1;
			entries.push_back(newEntry);
		}
	}

	/*
	 * Sort myEntryArray based on so enum value.
	 * Please provide a solution that sorts according to the enum
	 * The presence of the enum implies a switch statement based on its value
	 * You are provided with a myentry compare function in the cpp file
	 */

	bool compareAscending(const entry& x, const entry& y) {
		return x.word_uppercase < y.word_uppercase;
	}

	bool compareDescending(const entry& x, const entry& y) {
		return x.word_uppercase > y.word_uppercase;
	}

	bool compareNumberOccurrences(const entry& x, const entry& y) {
		return x.number_occurences > y.number_occurences;
	}

	void sort(vector<entry>  &entries, sortOrder so) {
		switch(so) {
		case sortOrder::ASCENDING :
			std::sort(entries.begin(), entries.end(), compareAscending);
			break;
		case sortOrder::DESCENDING :
			std::sort(entries.begin(), entries.end(), compareDescending);
			break;
		case sortOrder::NUMBER_OCCURRENCES :
			std::sort(entries.begin(), entries.end(), compareNumberOccurrences);
			break;
		case sortOrder::NONE :
			break;
		}
	}
}
