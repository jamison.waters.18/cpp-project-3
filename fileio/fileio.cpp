#include "../includes/fileio.h"
#include "../includes/array_functions.h"
#include "../includes/constants.h"

using namespace std;
using namespace constants;
using namespace KP;

/*if you are debugging the file must be in the project parent directory
  in this case Project2 with the .project and .cProject files*/
bool openFile(fstream& myfile, const string& myFileName,
		ios_base::openmode mode) {
	myfile.open(myFileName, mode);
	if (!myfile.is_open()) {
		return false; }
	return true;
}

/*iff myfile is open then close it*/
void closeFile(fstream& myfile) {
	if (myfile.is_open()) {
		myfile.close(); }
}

/* serializes all content in entries to file outputfilename
 * check out utils for helpful type conversion functions
 * returns  FAIL_FILE_DID_NOT_OPEN if cannot open outputfilename
 * 			FAIL_NO_ARRAY_DATA if there are 0 entries in myEntryArray
 * 			SUCCESS if all data is written and outputfilename closes OK
 * */
int writetoFile(vector<entry>  &entries, const string &outputfilename) {
	if (!getSize(entries)) {
		return FAIL_NO_ARRAY_DATA; }

	fstream myfile;
	if (!openFile(myfile, outputfilename, std::ios_base::out)) {
		return FAIL_FILE_DID_NOT_OPEN; }

	for (int i = 0; i < entries.size(); ++i) {
		myfile << entries[i].word << ' ' << entries[i].number_occurences;
		myfile << "\n";
	}

	closeFile(myfile);

	return SUCCESS;
}
